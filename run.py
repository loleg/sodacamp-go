import os, string
from flask import Flask, redirect
from pymongo import MongoClient
import pickle

app = Flask(__name__)

basedir = os.path.abspath(os.path.dirname(__file__))
app.config.from_object('config')

baseurl_home = "https://www.wuala.com/PublicDomain"
baseurl = "https://www.wuala.com/PublicDomain/%s/%s"

# Init Mongodb
database_name = app.config['MONGO_DATABASE']
client = MongoClient(
	host='mongodb://%(user)s:%(password)s@%(host)s:%(port)d/%(database)s' %
		dict(
			database=database_name,
			host=app.config['MONGO_HOST'],
			port=app.config['MONGO_PORT'],
			user=app.config['MONGO_USER'],
			password=app.config['MONGO_PASS']))
db = client[database_name]

def getIdentifier(name, werk):
    if name == "" or name is None: 
    	return (None, None)
    name = getShortText(name, 11)
    werk = getShortText(werk, 5)
    if werk is None: werk = "_"
    return (name, werk)

def getShortText(text, limit=10):
    if text is None or text == "": return None
    if not isinstance(text, unicode): text = unicode(text)
    s = "".join([c for c in text 
        if c in string.letters 
        or c in string.digits 
        or c in string.whitespace])
    s = s.lower().replace(" ", "")
    return s[:limit]

def getPdgIndex():
	""" Index Public Domain Game entries """
	index = {}
	if os.path.isfile('pdgindex.pickle'):
		print "Loading index from cache"
		return pickle.load(open('pdgindex.pickle', 'rb'))
	for coll in db.collection_names():
		print "Loading %s" % coll
		if not 'gestorben' in coll: continue
		for entry in db[coll].find().limit(10000):
			obj_id = str(entry['_id'])
			obj_name, obj_werk = getIdentifier(entry['Name'], entry['Werk'])			
			if obj_name is None:
				print entry
			else:
				#print "%s -> %s/%s" % (obj_id, obj_name, obj_werk)
				if not obj_name in index:
					index[obj_name] = {}
				index[obj_name][obj_werk] = [ coll, obj_id, entry ]
	print "Indexing complete."
	pickle.dump(index, open('pdgindex.pickle', 'wb'))
	return index

# Example entry:
# {u'Kategorie Autor': u'Maler', u'Name': u'Hans Christiansen', u'Format': None,
# u'Kategorie Werk': None, u'Werkbeschreibung': None, 
# u'Beschreibung': u'deutscher Maler und Kunsthandwerker (* 1866)', 
# u'Archiv': u'Ebay', u'Link': None, u'Gestorben in': u'Wiesbaden', 
# u'Werk': u'Titelblatt Jugend', u'_id': ObjectId('551bde57da6d3958791b0f03'), 
# u'Todesdatum': u'05.01.1945'}

pdgIndex = getPdgIndex()
print "%d objects loaded" % len(pdgIndex)

@app.route('/pdg/<name>/<werk>')
def pdg(name, werk):
	""" Look for a selected item in the index and redirect """
	if not name in pdgIndex: 
		shortname = getShortText(name, 20)
		print "Trying shortname: %s" % shortname
		if shortname in pdgIndex:
			name = shortname
		else:
			print "Author not found: %s" % name
			return redirect(baseurl_home, code=302)
	if getShortText(werk) in pdgIndex[name]:
		werk = getShortText(werk)
	if not werk in pdgIndex[name]: 
		first_werk = None
		for w in pdgIndex[name]:
			first_werk = w
			if werk in w: werk = w
		if not werk in pdgIndex[name]:
			if first_werk is not None:
				werk = first_werk
			else:
				print "Object not found: %s" % werk
				return redirect(baseurl_home, code=302)
	ref = pdgIndex[name][werk]
	coll, obj_id, entry = ref[0], ref[1], ref[2]
	#return redirect(app.config['BASEURL_PDG'] % (coll, obj_id), code=302)
	# Get date from collection
	try:
		collnum = int("".join([c for c in coll if c in string.digits]))
		godate = collnum
	except ValueError:
		godate = None
	if godate is None and 'Todesdatum' in entry:
		if '.' in entry['Todesdatum']:
			godate = entry['Todesdatum'].split('.')[-1]
		else:
			try:
				collnum = int("".join([c for c in entry['Todesdatum'] if c in string.digits]))
				godate = collnum
			except ValueError:
				godate = None
	if godate is None:
		print "Date not found in entry"
		return redirect(baseurl_home, code=302)
	if not 'Name' in entry:
		print "Name not found in entry"
		return redirect(baseurl_home, code=302)
	goauth = entry['Name'].replace(',', '')
	# if ',' in goauth:
	# 	gg = goauth.split(',')
	# 	goauth = "%s %s" % (gg[-1].strip(), gg[0].strip())
	targeturl = baseurl % (godate, goauth)
	print "Redirecting to %s" % targeturl
	return redirect(targeturl, code=302)

# Run the development server
if __name__ == '__main__':
	app.run(host='0.0.0.0', port=8080, debug=True)